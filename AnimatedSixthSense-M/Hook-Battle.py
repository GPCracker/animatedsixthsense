#*****
# Battle Hooks
#*****
def new_Battle_init(self):
	result = old_Battle_init(self)
	if (not hasattr(self, 'XAnimatedSixthSenseGUI') or self.XAnimatedSixthSenseGUI is None) and _config_['sixthSenseIcon']['enabled']:
		animationWidget = AnimationWidget(_config_['sixthSenseIcon']['size'], _config_['sixthSenseIcon']['position'])
		animationProvider = AnimationProvider(_config_['sixthSenseIcon']['animation'])
		self.XAnimatedSixthSenseGUI = Animation(animationProvider, animationWidget)
	if not hasattr(self, 'XAnimatedSixthSenseSound') or self.XAnimatedSixthSenseSound is None:
		self.XAnimatedSixthSenseSound = SoundEvent(Sound(_config_['sixthSenseSound']))
	return result

def new_Battle_showSixthSenseIndicator(self, isShow):
	if hasattr(self, 'XAnimatedSixthSenseGUI') and self.XAnimatedSixthSenseGUI is not None:
		if self.XAnimatedSixthSenseGUI.isPlaying:
			self.XAnimatedSixthSenseGUI.stopAnimation()
		self.XAnimatedSixthSenseGUI.playAnimation()
	if hasattr(self, 'XAnimatedSixthSenseSound') and self.XAnimatedSixthSenseSound is not None:
		self.XAnimatedSixthSenseSound()
	return None
