#*****
# Configuration
#*****
_config_ = None

#*****
# Default configuration
#*****
def defaultConfig():
	return {
		'modEnabled': ('Bool', True),
		'ignoreClientVersion': ('Bool', False),
		'hookSetTimeout': ('Float', 3.0),
		'modLoadedMessage': ('WideString', u'{} loaded.'.format(__application__)),
		'modUpdateMessage': ('WideString', u'Please update {}!'.format(__application__)),
		'sixthSenseSound': ('String', ''),
		'sixthSenseIcon': {
			'enabled': ('Bool', True),
			'size': ('Vector2', Math.Vector2(0, 0)),
			'position': ('Vector3', Math.Vector3(0, 0.6, 1)),
			'animation': ('String', '')
		}
	}

#*****
# Read configuration from file
#*****
def readConfig():
	mainSection = ResMgr.openSection(__file__.replace('.pyc', '.xml'))
	if mainSection is None:
		print '[{}] Config loading failed.'.format(__appShortName__)
	else:
		print '[{}] Config successfully loaded.'.format(__appShortName__)
	return ConfigReader().readSection(mainSection, defaultConfig())
