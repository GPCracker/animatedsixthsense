#*****
# Injecting Hooks
#*****
def injectHooks():
	from gui.Scaleform.Battle import Battle
	global old_Battle_init
	old_Battle_init = Battle.__init__
	Battle.__init__ = new_Battle_init
	Battle.showSixthSenseIndicator = new_Battle_showSixthSenseIndicator
	return None
