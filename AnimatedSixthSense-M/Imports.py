__application__ = 'Animated Sixth Sense'
__appShortName__ = 'AnimatedSixthSense'
__authors__ = ['GPCracker']
__version__ = '0.0.3'
__clientVersion__ = '0.9.7'
__status__ = 'Alpha #1'

if __name__ == '__main__':
	modInfo = '[{}] {} v{} {} by {} (WOT Client {}).'.format(__appShortName__, __application__, __version__, __status__, ', '.join(__authors__), __clientVersion__)
	print modInfo
	from time import sleep
	sleep(len(modInfo) * (4.0 / 70))
	exit()

import BigWorld
import ResMgr
import Math
import FMOD
import GUI
