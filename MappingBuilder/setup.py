# -*- coding: utf-8 -*-
from distutils.core import setup
import py2exe, sys, os

sys.argv.append('py2exe')
print sys.path.append('C:\\Windows\\system32')

def getDirFiles(path):
	return filter(os.path.isfile, [os.path.join(path, item) for item in os.listdir(path)])

setup(
	windows = [
		{
			'script': 'MappingBuilder.py',
			'icon_resources': [(1, 'MappingBuilder.ico')],
			'name': 'Animated Sixth Sense Mapping Builder',
			'description': 'Средство построения файла маппинга для мода Animated Sixth Sense.'.decode('utf-8'),
			'version': '0.0.0.1',
			'author': 'GPCracker',
			'copyright': 'Copyright (c) 2014 GPCracker.'
		}
	],
	data_files = [
		('',
			[
				r'C:\Python27\Lib\site-packages\PyQt4\QtCore4.dll',
				r'C:\Python27\Lib\site-packages\PyQt4\QtGui4.dll',
				r'C:\Python27\Lib\site-packages\PyQt4\QtNetwork4.dll'
			]
		),
		('imageformats', getDirFiles(r'C:\Python27\Lib\site-packages\PyQt4\plugins\imageformats'))
	],
	zipfile = None,
	options = {
		'py2exe': {
			'bundle_files': 1,
			'compressed': True,
			"includes":	['sip'],
			'dll_excludes': ['w9xpopen.exe', 'MSVCP90.dll', 'QtCore4.dll', 'QtGui4.dll', 'QtNetwork4.dll']
		}
	}
)
