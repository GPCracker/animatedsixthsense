# -*- coding: utf-8 -*-
#**********
# Import modules
#**********
import sys
from PyQt4 import QtGui, QtCore
from Ui.MainWindow import MainWindow
from Ui.Strings import Strings

#**********
# Defining application classes
#**********
class Application(QtGui.QApplication):
	@staticmethod
	def __getTextureMapping(x, y, n):
		getTextureMapping = lambda imagesMapping: '''<?xml version="1.0" encoding="utf-8"?>\n<!-- *** Created by GPCracker's Animated Sixth Sense Mapping Builder. *** -->\n<root>\n{imagesMapping}</root>'''.format(imagesMapping = ''.join(imagesMapping))
		getImageMapping = lambda (row, col, deltaX, deltaY): '''\t<image>\n\t\t<topLeft>{x1:.5f} {y1:.5f}</topLeft>\n\t\t<bottomLeft>{x1:.5f} {y2:.5f}</bottomLeft>\n\t\t<bottomRight>{x2:.5f} {y2:.5f}</bottomRight>\n\t\t<topRight>{x2:.5f} {y1:.5f}</topRight>\n\t</image>\n'''.format(x1 = col * deltaX, y1 = row * deltaY, x2 = (col + 1) * deltaX, y2 = (row + 1) * deltaY)
		return getTextureMapping(map(getImageMapping, [(index / x, index % x, 1.0 / x, 1.0 / y) for index in xrange(min(x * y, n))]))

	@staticmethod
	def __writeFile(file, content):
		with open(file, 'wt') as f:
			f.write(content)
		return None

	def saveMappingFile(self, file, x, y, n):
		self.__writeFile(file, self.__getTextureMapping(x, y, n))
		return None

	def __init__(self, *args, **kwargs):
		result = super(Application, self).__init__(*args, **kwargs)
		self.mainWindow = MainWindow()
		self.aboutToQuit.connect(self.__onQuit)
		return result

	def __onQuit(self):
		return None

	def __del__(self):
		return None

#**********
# Load Application
#**********
g_application = Application(sys.argv)
g_application.mainWindow.show()
sys.exit(g_application.exec_())
