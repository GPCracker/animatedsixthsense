import os, sys
if __name__ == '__main__':
	for root, dirs, files in os.walk('.'):
		for file in files:
			inputFile = os.path.join(root, file)
			if os.path.exists(file) and os.path.isfile(file) and file.endswith('.qrc'):
				outputFile = inputFile.replace('.qrc', '_rc.py')
				print '"{}" -> "{}"'.format(inputFile, outputFile)
				os.system('pyrcc4 "{}" -o "{}"'.format(inputFile, outputFile))
			elif os.path.exists(file) and os.path.isfile(file) and file.endswith('.ui'):
				outputFile = inputFile.replace('.ui', '_ui.py')
				print '"{}" -> "{}"'.format(inputFile, outputFile)
				os.system('pyuic4 "{}" -o "{}"'.format(inputFile, outputFile))
print 'All files compiled.'
os.system('pause')