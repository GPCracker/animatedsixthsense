# -*- coding: utf-8 -*-
#**********
# Import modules
#**********
from PyQt4 import QtGui, QtCore

#**********
# Strings
#**********
class Strings(object):
	SAVE_AS = QtCore.QString.fromUtf8("Сохранить как")
	XML_FILES = QtCore.QString.fromUtf8("Файлы XML (*.xml)")
