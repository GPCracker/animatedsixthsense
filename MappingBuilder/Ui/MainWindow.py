# -*- coding: utf-8 -*-
#**********
# Import modules
#**********
from PyQt4 import QtGui, QtCore
from MainWindow_ui import Ui_MainWindow
from Strings import Strings

#**********
# Defining application classes
#**********
class MainWindow(QtGui.QMainWindow):
	@staticmethod
	def getAppInstance():
		return QtCore.QCoreApplication.instance()

	def __init__(self, *args, **kwargs):
		result = super(MainWindow, self).__init__(*args, **kwargs)
		self.__setupUi()
		return result

	def __setupUi(self):
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.ui.spinBoxX.valueChanged.connect(self.__onSpinBoxValueChanged)
		self.ui.spinBoxY.valueChanged.connect(self.__onSpinBoxValueChanged)
		self.ui.pushButtonCompile.clicked.connect(self.__onCompileButtonClicked)
		return None

	def __onSpinBoxValueChanged(self, value):
		self.ui.spinBoxN.setMaximum(self.ui.spinBoxX.value() * self.ui.spinBoxY.value())
		return None

	def __onCompileButtonClicked(self):
		file = QtGui.QFileDialog.getSaveFileName(self, Strings.SAVE_AS, '.', Strings.XML_FILES)
		if file:
			self.getAppInstance().saveMappingFile(file, self.ui.spinBoxX.value(), self.ui.spinBoxY.value(), self.ui.spinBoxN.value())
		return None

	def __del__(self):
		return None
