class TextureProvider(object):
	@staticmethod
	def loadTexture(texturePath):
		mappingReader = ConfigReader()
		encodeMapping = lambda mappingData: (mappingData['topLeft'], mappingData['bottomLeft'], mappingData['bottomRight'], mappingData['topRight'])
		defaultImageMapping = {
			'topLeft': ('Vector2', Math.Vector2(0, 0)),
			'bottomLeft': ('Vector2', Math.Vector2(0, 1)),
			'bottomRight': ('Vector2', Math.Vector2(1, 1)),
			'topRight': ('Vector2', Math.Vector2(1, 0))
		}
		defaultTextureMapping = ('TextureMapping', [defaultImageMapping])
		mappingReader.extTypes = {
			'TextureMapping': lambda *args: mappingReader.readList(*args, itemName = 'image', itemType = 'ImageMapping', itemDefault = defaultImageMapping),
			'ImageMapping': lambda *args: encodeMapping(mappingReader.readDict(*args))
		}
		return BigWorld.PyTextureProvider(texturePath), mappingReader.readConfig(texturePath.replace('.dds', '.xml'), defaultTextureMapping)
	
	def __init__(self, texturePath):
		self.texturePath = texturePath
		self.texture, self.mapping = self.loadTexture(texturePath)
		return None
	
	def __repr__(self):
		return 'TextureProvider(texturePath={})'.format(repr(self.texturePath))

class AnimationFrame(object):
	@staticmethod
	def decodeColorHex(colorHex):
		return Math.Vector4([int(colorHex[start:start+2], 16) for start in xrange(0, len(colorHex), 2)])
	
	@staticmethod
	def parseFrameData(frameData):
		import re
		textureAlias, frameIndex, overlayColor = re.split(r'[:#]+', frameData)
		return textureAlias, int(frameIndex), AnimationFrame.decodeColorHex(overlayColor)
	
	def __init__(self, frameData):
		self.frameData = frameData
		self.textureAlias, self.frameIndex, self.overlayColor = self.parseFrameData(frameData)
		return None
	
	def __repr__(self):
		return 'AnimationFrame(frameData={})'.format(repr(self.frameData))

class AnimationProvider(object):
	@staticmethod
	def loadAnimation(animationPath):
		animationReader = ConfigReader()
		defaultTexture = ''
		defaultFrameData = ''
		defaultFrameSequence = []
		defaultAnimation = {
			'textures': ('AliasesDict', {}),
			'interval': ('Float', 1.0),
			'frames': ('FrameSequence', [])
		}
		def readFrameSequence(xml, default):
			if xml is not None:
				loopCount = animationReader.readSection(xml['count'], ('Int', 1))
				loopFrames = []
				for sectionName, xmlSection in xml.items():
					if sectionName == 'frame':
						loopFrames.append(AnimationFrame(animationReader.readSection(xmlSection, ('String', defaultFrameData))))
					elif sectionName == 'loop':
						loopFrames += readFrameSequence(xmlSection, defaultFrameSequence)
				return loopFrames * loopCount
			return default
		animationReader.extTypes = {
			'AliasesDict': lambda *args: animationReader.readCustomDict(*args, itemType = 'Texture', itemDefault = defaultTexture),
			'Texture': lambda *args: TextureProvider(animationReader.readBase('String', *args)),
			'FrameSequence': lambda *args: readFrameSequence(*args)
		}
		return animationReader.readConfig(animationPath, defaultAnimation)
	
	def __init__(self, animationPath):
		self.animationPath = animationPath
		self.animationData = self.loadAnimation(animationPath)
		return None
	
	def __repr__(self):
		return 'AnimationProvider(animationPath={})'.format(repr(self.animationPath))

class AnimationWidget(object):
	def __init__(self, size = (0, 0), position = (0, 0, 1)):
		self.__simpleGUIComponent = GUI.Simple('')
		self.__simpleGUIComponent.verticalAnchor = 'CENTER'
		self.__simpleGUIComponent.horizontalAnchor = 'CENTER'
		self.__simpleGUIComponent.verticalPositionMode = 'CLIP'
		self.__simpleGUIComponent.horizontalPositionMode = 'CLIP'
		self.__simpleGUIComponent.widthMode = 'PIXEL'
		self.__simpleGUIComponent.heightMode = 'PIXEL'
		self.__simpleGUIComponent.materialFX = 'BLEND'
		self.__simpleGUIComponent.size = size
		self.__simpleGUIComponent.position = position
		self.__simpleGUIComponent.visible = False
		GUI.addRoot(self.__simpleGUIComponent)
		return None
	
	def setFrame(self, texture, mapping, colour):
		self.__simpleGUIComponent.texture = texture
		self.__simpleGUIComponent.mapping = mapping
		self.__simpleGUIComponent.colour = colour
		return None
	
	def setVisibility(self, isVisible):
		self.__simpleGUIComponent.visible = isVisible
		return None
	
	def __del__(self):
		GUI.delRoot(self.__simpleGUIComponent)
		return None

class Animation(object):
	@staticmethod
	def getFrameData(animationProvider, animationFrameIndex):
		animationFrame = animationProvider.animationData['frames'][animationFrameIndex]
		textureProvider = animationProvider.animationData['textures'][animationFrame.textureAlias]
		return textureProvider.texture, textureProvider.mapping[animationFrame.frameIndex], animationFrame.overlayColor
	
	def __init__(self, animationProvider, animationWidget):
		self.__animationProvider = animationProvider
		self.__animationWidget = animationWidget
		self.__animationCallbacks = []
		return None
	
	@property
	def isPlaying(self):
		return bool(self.__animationCallbacks)
	
	def playAnimation(self):
		assert not self.isPlaying, 'Animation already started.'
		framesQuantity = len(self.__animationProvider.animationData['frames'])
		changeInterval = self.__animationProvider.animationData['interval']
		self.__animationCallbacks.append(Callback(0.0, self.__animationWidget.setVisibility.im_func, self.__animationWidget.setVisibility.im_self, True))
		for animationFrameIndex in xrange(framesQuantity):
			self.__animationCallbacks.append(Callback(changeInterval * animationFrameIndex, self.__animationWidget.setFrame.im_func, self.__animationWidget.setFrame.im_self, *self.getFrameData(self.__animationProvider, animationFrameIndex)))
		self.__animationCallbacks.append(Callback(changeInterval * framesQuantity, self.stopAnimation.im_func, self.stopAnimation.im_self))
		return None
	
	def stopAnimation(self):
		assert self.isPlaying, 'Animation has not been started.'
		self.__animationCallbacks = []
		self.__animationWidget.setVisibility(False)
		return None
	
	def __del__(self):
		return None
