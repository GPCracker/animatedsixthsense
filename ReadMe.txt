#Disclaimer:
#	This file was written in Russian, and partially translated by special software.
#	Configuration file was translated by author himself in case of preventing data distortion.
#	Author is not responsible for the accuracy of any other translations except Russian below.

Анимированное "Шестое чувство" / Animated Sixth Sense Mod
Модификация для World of Tanks / Modification for World of Tanks

Автор / Author: GPCracker

[Описание модификации]
Модификация позволяет заменить статичную иконку лампочки "Шестого чувства" на анимированную, а также добавляет звук на срабатывание "Шестого чувства". Анимация задается последовательностью текстур, сменяющихся с определенным интервалом. Последовательность кадров и интервал устанавливаются в файле анимации. В одной текстуре может быть несколько кадров, их границы задаются в файле маппинга. В основном файле конфигурации указываются размеры иконки "Шестого чувства" на экране и его координаты, а также путь к файлу анимации и звуковому событию.

[Важная информация]
Данный мод отключает лампочку "Шестого чувства", выводимую средствами Flash.
Загрузчики скриптов с модификацией не поставляются. Вы ищете и устанавливаете их самостоятельно.
Файлы настроек анимации и маппинга создаются авторами анимаций, поэтому комментариев в них нет.
В комплектации данного мода присутствуют только текстуры стандартной иконки и скрипты. Звуков нет.

[Редактирование файла конфигурации]
Файл конфигурации нельзя редактировать стандартным блокнотом, входящим в Windows, а также другими простыми текстовыми редакторами, не поддерживающими кодировку файла конфигурации - UTF-8 w/o BOM.
Все параметры файла конфигурации имеют комментарии на русском и английском языках. Если у параметра нет комментария, значит этот параметр определен выше. Ищите одноименный параметр / секцию.

[Создание иконок]
*** Для создание своей анимации необходимы навыки работы с изображениями в графических редакторах, а также специальное ПО. ***
	***    Создание текстур и маппинга.    ***
Анимация использует базовый класс движка игры BigWorld - SimpleGUIComponent, немалое количество параметров унаследовано оттуда. Читайте документацию.
Скрипты данного мода позволяют использовать несколько текстур в одной анимации. Кроме того, доступен т.н. маппинг - в одной текстуре содержится не одно, а целая матрица изображений.
1. Требования к текстурам: 8.8.8.8 ARGB 32bpp, либо другой формат, поддерживаемый BigWorld.
2. Как использовать маппинг: в одной текстуре присутствует несколько изображений, рядом с текстурой под таким же именем (только с расширением xml) лежит файлик маппинга, который определяет границы каждого изображения. Даже если в текстуре одно изображение, файлик должен присутствовать обязательно.
3. Что обозначают цифры и поля в этом файле:
	<image></image> Блок одной картинки, причем в файле присутствует неявная нумерация, начинается с 0, по порядку этих блоков, назовем этот номер "номером кадра текстуры".
	topLeft, bottomLeft, bottomRight, topRight - это соответственно координаты верхнего левого, нижнего левого, правого нижнего, правого верхнего углов изображения. Координаты (X, Y). Координаты верхнего левого угла (0.0, 0.0), правого нижнего (1.0, 1.0). Подробности можно посмотреть в документации BigWorld.
4. Как создавать файл маппинга:
	а) Руками. Долго, неудобно, ... зато конфигурация изображений в текстуре может быть любой.
	б) Скриптом. Быстро, удобно, ... но есть требования к текстуре:
		1. Изображения должны быть одного размера
		2. Размер текстуры должен быть кратным размеру одного изображения
		3. Между изображениями, между изображением и краем текстуры не должно быть полей
		4. Изображения располагаются матрицей, нумерация построчная ("номер кадра текстуры"), слева направо, сверху вниз, начиная с 0.
		Как пользоваться скриптом:
		1. Указываете "размерность" по горизонтали и вертикали. "В изображениях". Пикселы смысла тут не имеют, почему - читай выше. То есть текстура, идущая в комплекте со скриптом, имеет размерность 4 на 4.
		2. Указываете, сколько реально изображений в файле.
		3. Скрипт создает маппинг файл.
		Как работает скрипт:
		1. Скрипт делит "условную клетку маппинга 1х1" по горизонтали и вертикали на указанное количество столбцов и строк. Нумерует прямоугольники / квадраты построчно сверху, начиная с 0.
		0	1	2	3
		4	5	6	7
		8	9	10	11
		12	13	14	15
		2. Затем в файл маппинга добавляется информация о границах первых N кадров, где N - число указанное Вами как реальное число кадров. Соответственно, если количество изображений не позволяет использовать матрицу с полным заполнением (в дефолтной анимации матрица 4х4, кадров 14), то пустыми следует оставлять ячейки с большими номерами, то есть последние. Пустые ячейки не создают каких-либо проблем для скрипта, но поскольку их нет в файле маппинга, то для скрипта их не существует, и попытка к ним обратиться вызовет ошибку. Не забывайте про нумерацию с нуля и следите за индексами в анимации.
5. При создании текстуры из нескольких изображений не забывайте про альфа-канал.
6. Как создавать анимацию:
	После изменений в версии 0.0.3 сильно изменилась структура файла анимации. Старые файлы работать не будут.
	1. Для начала необходимо указать, какие текстуры будут использоваться в анимации (необходимо для их загрузки до воспроизведения анимации), а так же сокращенные имена, по которым будет осуществляться обращение к ним. В качестве имени параметра в секции textures используется сокращенное имя, значение - путь к текстуре. Если в течение воспроизведения анимации будет попытка обращения к необъявленной текстуре, возникнет ошибка. Следите за объявлением текстур.
	2. Далее задается интервал смены текстур. Хотя чаще всего он настраивается уже после настройки последовательности кадров. Параметр interval.
	3. Следующим шагом является настройка последовательности кадров. Кадр объявляется параметром frame, значением является условное имя текстуры (шаг 1), номер изображения в этой текстуре, а также цвет, накладываемый на эту текстуру + прозрачность в формате RGBA. Внутри секции frames допустимо также использовать тег loop, позволяющий несколько раз воспроизводить вложенную в него последовательность кадров, количество повторов определяется параметром count. Например, так:
	<frames>
		<count>2</count>
		<loop>
			<frame>test.dds:0#FFFFFFFF</frame>
			<frame>test.dds:1#FF0000CC</frame>
			<count>3</count>
		</loop>
	</frames>
	В данном примере будет шесть раз показана последовательность из двух кадров. Стоит также отметить, что поведение секции frames аналогично поведению секции loop. Что касается параметра count, то при его отсутствии он принимается равным единице (воспроизведение последовательности 1 раз).
	4. Наложение цвета осуществляется следующим образом: если изображение белое, то оно будет накладываемого цвета. При наложении белого цвета на любое изображение ничего не происходит. Непрозрачность (последний блок) накладывается отдельно, если FF, то прозрачность изображения не меняется, если 00 - полная прозрачность, в других случая осуществляется умножение. Данный алгоритм является частью BigWorld, читайте документацию.
7. Настройка главного файла конфигурации:
	Все пути к текстурам определяются в файле анимации. Файл маппинга должен располагаться рядом с текстурой и иметь соответствующее имя. В главном файле конфигурации указывается только путь к файлу анимации, а так же размеры иконки.
	Установка двух нулей в размере иконки подразумевает использование размеров текстуры, поэтому некорректно работает при использовании маппинга. Установка одного нуля подразумевает масштабирование этого размера с целью сохранения пропорций при изменении другого размера, работает корректно только если у вас используется квадратная матрица изображений. Во всех остальных случаях необходимо указывать оба размера и следить за сохранением пропорций. Иконку можно уменьшать или увеличивать на экране без изменения текстуры, однако не забывайте, что она растровая. Параметр унаследован от BigWorld.
	Позиционирование иконки задается по типу CLIP, координаты правого верхнего угла (1, 1), левого нижнего (-1, -1). Параметр унаследован от BigWorld.

[Description of modification]
Modification allows to replace a static icon of a bulb of "Sixth sense" on animated, and also adds a sound on operation of "Sixth sense". Animation is set by sequence of the textures which are replaced with a certain interval. The sequence of frames and an interval are established in the file of animation. In one texture there can be some frames, their borders are set in the mapping file. The sizes of an "Sixth sense" icon on the screen are specified in the main configuration file and its coordinates, and also a path to the animation file and a sound event.

[Important information]
Given modification disconnects the bulb of "Sixth sense" displayed by means of Flash.
Loaders of scripts with modification aren't delivered. You look for and you establish them independently.
Files of animation settings and mapping are created by authors of animations therefore comments in them aren't present.
At a complete set given modification there are only textures of a standard icon and scripts. Sounds aren't present.

[Editing configuration file]
It is impossible to edit the configuration file with the standard notepad included in Windows, and also with other simple text editors which aren't supporting the encoding of the configuration file - UTF-8 w/o BOM.
All parameters of the configuration file have comments in the Russian and English languages. If parameter has no comment, this parameter means is determined above. Look for the parameter / section of the same name.

[Creation of icons]
*** For creation of the animation skills of work with images in graphic editors required, and also special  software. ***
 *** Creation of textures and mapping. ***
Animation uses a basic class of the game engine BigWorld - SimpleGUIComponent, the considerable number of parameters is inherited from there. Read documentation.
Scripts of given modification allow to use some textures in one animation. Besides, the so-called mapping is available - one texture contains not one, but the whole matrix of images.
1. Requirements to textures: 8.8.8.8 ARGB 32bpp, or other format supported by BigWorld.
2. How to use a mapping: at one texture there are some images, near texture under the same name (only with the xml extension) the file of a mapping which defines limits of each image lies. Even if one image, a file has to be present at texture surely.
3. That designate figures and fields in this file:
	<image>  Blok of one picture, and is present at the file implicit numbering, begins with 0, in the order of these blocks, we will call this number "number of a image of texture".
	topLeft, bottomLeft, bottomRight, topRight is respectively a coordinate top left, lower left, right lower, right top image corners. Coordinates (X, Y). Coordinates of the top left corner (0.0, 0.0), right lower (1.0, 1.0). The detail can look in documentation of BigWorld.
4. How to create the mapping file:
	a) By hands. Long, inconveniently... but the configuration of images in texture can be any.
	b) Script. Quickly, conveniently... but there are requirements to texture:
		1. Images have to be one size
		2. The extent of texture has to be multiple to the size of one image
		3. Between images, between the image and edge of texture there shouldn't be fields
		4. Images settle down a matrix, numbering line-by-line ("number of a image of texture"), from left to right, from top to down, since 0.
		How to use a script:
		1. You specify "dimension" across and verticals. "In images". Sense pixels don't make here why - read above. That is the texture going in a set with a script has dimension 4 on 4.
		2. You specify, how many it is real images in the file.
		3. The script creates a mapping the file.
		As the script works:
		1. The script divides "a conditional cell of a mapping 1х1" across and verticals into the specified quantity of columns and lines. Numbers rectangles / squares line-by-line from above, since 0.
		0	1	2	3
		4	5	6	7
		8	9	10	11
		12	13	14	15
		2. Then information on borders of the first N images, where by N - the number specified by you as real number of images is added to the file of a mapping. Respectively, if the number of images doesn't allow to use a matrix with full filling (in default animation a matrix 4х4, 14 frames), empty it is necessary to leave cells with big numbers, that is the last. Empty cells don't create any problems for a script but as they aren't present in the file of a mapping, for their script doesn't exist, and attempt an error will cause to address to them. Don't forget about numbering from 0 and you watch indexes in animation.
5. At creation of texture from several images don't forget about the alpha channel.
6. How to create animation:
	After changes in version 0.0.3 the structure of the file of animation strongly changed. Old files won't work.
	1. For a start it is necessary to specify, what textures will be used in animation (it is necessary for their loading before animation reproduction), and also reduced names on which the address to them will be carried out. As a parameter name in the section 'textures' the reduced name, value - a path to texture is used. If during reproduction of animation there is an attempt of the appeal to undeclared texture, there will be an error. You watch the announcement of textures.
	2. Further the interval of change of textures is set. Though most often it is adjusted after control of sequence of frames. Parameter name 'interval'.
	3. The following step is control of sequence of frames. The sframe appears the 'frame' parameter, value is the conditional name of texture (a step 1), number of the image in this texture, and also the color imposed on this texture + opacity in the RGBA format. In the section 'frames' is also admissible to use the 'loop' tag allowing to reproduce several times the sequence of frames enclosed in it, the number of repetitions is defined by the 'count' parameter. For example, so:
	<frames>
		<count>2</count>
		<loop>
			<frame>test.dds:0#FFFFFFFF</frame>
			<frame>test.dds:1#FF0000CC</frame>
			<count>3</count>
		</loop>
	</frames>
	In this example six times the sequence of two frames will be shown. It is also worth noting that the behavior of the section 'frames' is similar to behavior of the section 'loop'. As for the 'count' parameter, at its absence it is accepted equal to 1 (reproduction of sequence of 1 times).
	4. Imposing of color is carried out as follows: if the image white, it is the imposed color. When imposing white color on any image occurs nothing. Opacity (the last block) is imposed separately if FF, transparency of the image doesn't change if 00 - full transparency, in others of a case is carried out multiplication. This algorithm is part of BigWorld, read documentation.
7. Setting up the main file of a configuration:
	All ways to textures are defined in the file of animation. The file of a mapping has to settle down near texture and have the corresponding name. Only the way to the file of animation, and also the icon sizes is specified in the main file of a configuration.
	Installation of two zero at a size of an icon means use of the size of texture therefore incorrectly works when using a mapping. Installation of one zero means scaling of this size for the purpose of preservation of proportions at change of other size, works correctly only if you use a square matrix of images. It is necessary to specify both sizes in all other cases and to watch preservation of proportions. The icon can be reduced or increased on the screen without change of texture, however don't forget that it is raster. Parameter is inherited from BigWorld.
	Positioning of an icon is set as CLIP, the coordinate of the right top corner (1, 1), left lower (-1,-1). Parameter is inherited from BigWorld.
